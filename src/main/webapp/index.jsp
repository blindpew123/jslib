<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
    <c:if test="${empty libPage}">
        <jsp:useBean id="libPage" class="tk.blindpew123.models.LibPage" />
        <c:set target="${libPage}" property="title" value="Библиотека Java Shool" />
        <c:set target="${libPage}" property="contentURL" value="/WEB-INF/jsp/view/News.jsp" />
    </c:if>
    <head><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
        <title><c:out value="${libPage.title}" /></title>
    </head>
    <body>
         <jsp:include page="WEB-INF/jsp/view/Menu.jsp" flush="true" />
         <jsp:include page="${libPage.contentURL}" flush="true" />
         <jsp:include page="footer.jsp" />

         <script type="text/javascript">
             $(document).ready(function(e) {
                 var h = $('nav').height() + 20;
                 $('body').animate({ paddingTop: h });
             });
         </script>
    </body>
</html>
