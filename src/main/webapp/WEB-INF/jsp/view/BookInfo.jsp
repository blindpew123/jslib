<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="/WEB-INF/jsp/view/BookDetail.jsp" flush="true" />
</br>
</br>
<div class="container">
    <div class="row">
        <div class="col">
            <c:choose>
                <c:when test="${issued}">
                    <a class="btn btn-primary" href="<c:url value='/returnBook' />">Вернуть книгу</a>
                </c:when>
                <c:otherwise>
                    <a class="btn btn-primary" href="<c:url value='/issueBook' />">Выдать книгу</a>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<c:if test="${not empty debt}">
    <div class="container">
        <div class="row">
            <div class="col"><h5 style="color: red">Долг составляет: ${debt}</h5></div>
        </div>
    </div>
</c:if>



