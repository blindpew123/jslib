<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="container">
    <div class="row">
        <div class="col">
            <form:form  action="${studentFormUrl}" method="post" modelAttribute="student" acceptCharset="UTF-8">
                <div class="form-group">
                    <form:label path="firstName">Имя ученика</form:label>
                    <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="firstName" />
                    <div class="invalid-feedback">
                        <form:errors path='firstName' />
                    </div>
                </div>
                <div class="form-group">
                    <form:label path="lastName">Фамилия ученика</form:label>
                    <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="lastName" />
                    <div class="invalid-feedback">
                        <form:errors path='lastName' />
                    </div>
                </div>
                <div class="form-group">
                    <form:label path="grade.grade">Класс</form:label>
                    <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="grade.grade" />
                    <div class="invalid-feedback">
                        <form:errors path='grade.grade' />
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value="OK" />
            </form:form>
        </div>
    </div>
</div>
