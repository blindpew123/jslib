<%@ page contentType="text/html;charset=UTF-8" language= "java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:choose>
    <c:when test="${not empty book}" >
        <jsp:include page="/WEB-INF/jsp/view/BookDetail.jsp" flush="true" />
        <!--здесь ошибка поиска студента-->
            <c:if test="${not empty studentError}">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-danger" role="alert">${studentError}</div>
                        </div>
                    </div>
                </div>

            </c:if>
            <c:url value="/issueBook" var="studentFormUrl "/>
            <jsp:include page="/WEB-INF/jsp/view/StudentForm.jsp" flush="true" />

    </c:when>
    <c:otherwise>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="alert alert-danger" role="alert">Книга не выбрана</div>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
