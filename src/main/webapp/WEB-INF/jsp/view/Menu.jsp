<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="currentURI" value="${requestScope['javax.servlet.forward.servlet_path']}" />
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Библиотека Java School</a>
    <button class="navbar-toggler" type="button" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class='nav-item <c:if test="${currentURI == '/allBooks'}">active</c:if>'>
                <a class="nav-link" href="<c:url value='/allBooks' />">Каталог книг</a>
            </li>
            <li class='nav-item <c:if test="${currentURI == '/allStudents'}">active</c:if>'>
                <a class="nav-link" href="<c:url value='/allStudents' />">Список учеников</a>
            </li>
            <li class='nav-item <c:if test="${currentURI == '/addBook'}">active</c:if>'>
                <a class="nav-link" href="<c:url value='/addBook' />">Добавить книгу</a>
            </li>
            <li class='nav-item <c:if test="${currentURI == '/addStudent'}">active</c:if>'>
                <a class="nav-link" href="<c:url value='/addStudent' />">Добавить ученика</a>
            </li>
        </ul>
    </div>
</nav>


