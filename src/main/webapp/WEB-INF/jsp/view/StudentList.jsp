<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container">
<c:choose>
    <c:when test="${!list.isEmpty()}">
        <c:forEach items="${list}" var="item" varStatus="index">
            <div class="row">
                <div class="col"><a href="<c:url value='/studentInfo/${item.id}' />">${item.firstName}&nbsp${item.lastName}</a></div>
                <div class="col">${item.grade.grade}</div>
                </br>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
    <div class="row">
        <div class="col">
            <h2>По Вашему запросу ничего не найдено!</h2>
        </div>
    </div>
    </c:otherwise>
</c:choose>
</div>