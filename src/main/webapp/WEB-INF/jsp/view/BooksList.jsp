<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container">
<c:choose>
    <c:when test="${!list.isEmpty()}">
        <c:forEach items="${list}" var="item" varStatus="index">
            <div class="row">
            <div class="col"><a href="<c:url value="/bookInfo/${item.id}" />">${item.bookName}</a></div>
            <div class="col">${item.author.firstName}&nbsp${item.author.lastName}</div>
            <div class="col">${item.genre.genre}</div>
            <div class="col">${item.country.country}</div>
            <div class="col"><c:choose>
                    <c:when test="${not empty issueDetails}">
                        <div>Выдана:&nbsp${item.bookIssues.get(item.bookIssues.size()-1).issueDateTime}</div>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${item.bookIssues.size()==0}"><span style="color: green">В наличии</span></c:when>
                            <c:when test="${item.bookIssues.stream().filter(i->i.returnDateTime == null).count()==0}"><span style="color: green">В наличии</span></c:when>
                            <c:otherwise><span style="color: red">На руках</span></c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </div>

            </div>
            </br>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <div class="row">
            <div class="col"></div><h2>По Вашему запросу ничего не найдено!</h2></div>
        </div>
    </c:otherwise>
</c:choose>
</div>
