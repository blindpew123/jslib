<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:url value="/findBook" var="bookformUrl"/>
<jsp:include page="/WEB-INF/jsp/view/BookForm.jsp" flush="true" />
