<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class=”container”>
    <div class=”row”>
        <div class="col">
            <a class="btn btn-primary" href="<c:url value="/findStudent" />" role="button"><i class="fas fa-search"></i>&nbspНайти ученика</a>
        </div>
    </div>
</div>
</br>
<jsp:include page="/WEB-INF/jsp/view/StudentList.jsp" flush="true" />