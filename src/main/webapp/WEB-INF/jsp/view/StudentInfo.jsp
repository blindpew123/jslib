<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/jsp/view/StudentDetail.jsp" />
</br>
<div class="container">
    <div class="row">
        <div class="col"><h5>Книги на руках:</h5></div>
    </div>
</div>
<jsp:include page="/WEB-INF/jsp/view/BooksList.jsp" flush="true" />
</br>
</br>
<c:if test="${not empty debt}">
    <div class="container">
        <div class="row">
            <div class="col"><h5 style="color: red">Общий долг составляет: ${debt}</h5></div>
        </div>
    </div>
</c:if>
