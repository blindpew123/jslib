<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="container">
    <div class="row">
        <div class="col">
        <form:form  action="${bookformUrl}" method="post" modelAttribute="book" acceptCharset="UTF-8">
            <div class="form-group">
                <form:label path="bookName">Название</form:label>
                <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="bookName" />
                <div class="invalid-feedback">
                    <form:errors cssClass="help-inline" path='bookName' />
                </div>
            </div>
            <div class="form-group">
                <form:label path="author.firstName">Имя автора</form:label>
                <form:input cssClass="form-control"  cssErrorClass="form-control is-invalid" path="author.firstName" />
                <div class="invalid-feedback">
                    <form:errors path='author.firstName' cssClass="help-inline"/>
                </div>
            </div>
            <div class="form-group">
                <form:label path="author.lastName">Фамилия автора</form:label>
                <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="author.lastName" />
                <div class="invalid-feedback"
                    <form:errors path='author.lastName' cssClass="help-inline"/>
                </div>
            </div>
            <div class="form-group">
                <form:label path="genre.genre">Жанр</form:label>
                <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="genre.genre" />
                <div class="invalid-feedback">
                    <form:errors path='genre.genre' cssClass="help-inline"/>
                </div>
            </div>
            <div class="form-group">
                <form:label path="country.country">Страна</form:label>
                <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="country.country" />
                <div class="invalid-feedback">
                    <form:errors path='country.country' />
                </div>
            </div>
            <input type="submit" class="btn btn-primary" value="OK" />
        </form:form>
        </div>
    </div>
</div>
