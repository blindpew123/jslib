<div class="container">
    <div class="row">
        <div class="col">
            <span>
                Название книги
            </span>
            <span>
                <h4>${book.bookName}</h4>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span>
                Автор
            </span>
            <span>
                <h4>${book.author.firstName}&nbsp;${book.author.lastName}</h4>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <span>
                Жанр
            </span>
            <span>
                <h5>${book.genre.genre}</h5>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col">
        <span>
            Страна
        </span>
        <span>
            <h5>${book.country.country}</h5>
        </span>
        </div>
    </div>
</div>