DROP TABLE IF EXISTS books, authors, genres, countries, students, grades, issues;

CREATE TABLE books (
   bookid INT NOT NULL AUTO_INCREMENT
  ,bookname VARCHAR(60) NOT NULL
  ,author INT NOT NULL
  ,genre VARCHAR(20) NOT NULL
  ,country VARCHAR(20) NOT NULL
  ,PRIMARY KEY (bookid)
);
CREATE TABLE authors (
    authorid INT NOT NULL AUTO_INCREMENT
   ,authorfirstname VARCHAR(40) NOT NULL
   ,authorlastname VARCHAR(40) NOT NULL
   ,PRIMARY KEY (authorid)
);
CREATE TABLE genres (
   genre VARCHAR(20) NOT NULL
  ,PRIMARY KEY (genre)
);
CREATE TABLE countries (
   country VARCHAR(20)
  ,PRIMARY KEY (country)
);
CREATE TABLE students (
  studentid INT         NOT NULL AUTO_INCREMENT
  ,firstname VARCHAR(20)
  ,lastname  VARCHAR(20)
  ,grade VARCHAR(10) NOT NULL
  ,PRIMARY KEY (studentid)
);
CREATE TABLE grades (
   grade VARCHAR(10) NOT NULL
  ,PRIMARY KEY (grade)
);

CREATE TABLE issues (
    issueid INT NOT NULL AUTO_INCREMENT
    ,book INT NOT NULL
    ,student INT NOT NULL
    ,issuedatetime TIMESTAMP NOT NULL
    ,returndatetime TIMESTAMP NULL
    ,PRIMARY KEY (issueid)
);


