insert into books (bookname, author, genre, country)
values ('Thinking in Java 4-th ed.', '1', 'tutorial', 'USA');
insert into books (bookname, author, genre, country)
values ('Thinking in Java 2-th ed.', '1', 'tutorial', 'USA');
insert into books (bookname, author, genre, country)
values ('The Walking Dead, Vol. 1', '2', 'comics', 'USA');
insert into books (bookname, author, genre, country)
values ('Super Mario Adventures', '3', 'comics', 'Japan');
insert into books (bookname, author, genre, country)
values ('Ghost in the Shell', '4', 'comics', 'Japan');

insert into authors (authorfirstname, authorlastname)
values('Bruce','Eckel');
insert into authors (authorfirstname, authorlastname)
values('Robert','Kirkman');
insert into authors (authorfirstname, authorlastname)
values('Kentaro','Takemura');
insert into authors (authorfirstname, authorlastname)
values('Shirow','Masamune');

insert into genres(genre) values ('tutorial');
insert into genres(genre) values ('comics');

INSERT INTO countries (country) VALUES ('USA');
INSERT INTO countries (country) VALUES ('Japan');

INSERT INTO grades (grade) VALUES ('10А');
INSERT INTO grades (grade) VALUES ('10Б');
INSERT INTO grades (grade) VALUES ('11А');
INSERT INTO grades (grade) VALUES ('11Б');

INSERT INTO students (firstname, lastname, grade)
    VALUES ('Максим', 'Иванов','10А');
INSERT INTO students (firstname, lastname, grade)
    VALUES ('Иван', 'Максимов','11Б');

INSERT INTO issues (book, student, issuedatetime, returndatetime)
VALUES (2, 1,'1990-01-01 00:00:01', '1992-01-01 00:00:01');

INSERT INTO issues (book, student, issuedatetime, returndatetime)
VALUES (3, 2, '2016-01-01 00:00:01', NULL );

INSERT INTO issues (book, student, issuedatetime, returndatetime)
VALUES (4, 1, '2018-01-01 00:00:01', NULL );


