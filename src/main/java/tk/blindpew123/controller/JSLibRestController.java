package tk.blindpew123.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.data.*;
import tk.blindpew123.models.AngularIssueModel;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/rest")
public class JSLibRestController {

    private BooksDAO booksDAO;
    private StudentsDAO studentsDAO;
    private IssuesDAO issuesDAOImpl;


    @Autowired
    public void setBooksDAO(BooksDAO booksDAO) {
        this.booksDAO = booksDAO;
    }

    @Autowired
    public void setStudentsDAO(StudentsDAO studentsDAO) {
        this.studentsDAO = studentsDAO;
    }

    @Autowired
    public void setIssuesDAOimpl(IssuesDAO issuesDAOImpl) {
        this.issuesDAOImpl = issuesDAOImpl;
    }

    // Book API
    @GetMapping("/allBooks")
    public List<Book> getAllBook() {
        List<Book> list = booksDAO.getBooksByParam(null, true);
        System.out.println(Arrays.toString(list.toArray()));
        return list;
    }

    @GetMapping("/getBook/{id}")
    public Book getBook(@PathVariable("id") long id){
        return booksDAO.getBookById(id);
    }

    @PostMapping("/getBooksByTemplate")
    public List<Book> getBooksByTemplate(@RequestBody Book book){
        return booksDAO.getBooksByParam(book, true);
    }

    @GetMapping("/getIssuedBookForStudent/{id}")
    public List<Book> getIssuedBookForStudent(@PathVariable("id") long id){
        Student student = new Student();
        student.setId(1);
        return issuesDAOImpl.getAllBooksIsssuedTo(student, false);
    }

    @PostMapping("/addBook") //TODO: make real impl.
    public Book addBook(@RequestBody Book book){
        booksDAO.addBook(book);
        System.out.println(book);
        return book;
    }

    //----------------
    // Student API
    @GetMapping("/allStudents")
    public List<Student> getAllStudent() {
        return studentsDAO.findStudent(new Student());
    }

    @GetMapping("/getStudent/{id}")
    public Student getStudent(@PathVariable("id") long id){
        return studentsDAO.findStudentById(id);
    }

    @GetMapping("/getStudentFromIssue/{id}")
    public Student getStudentFromIssue(@PathVariable("id") long id){
        return issuesDAOImpl.getStudentFromIssue(id);
    }

    @PostMapping("/getStudentsByTemplate")
    public List<Student> getStudentsByTemplate(@RequestBody Student student){
        return studentsDAO.findStudent(student);
    }

    @PostMapping("/addStudent")
    public Student getStudentFromIssue(@RequestBody Student student){
        studentsDAO.addStudent(student);
        return student;
    }

    //---------------------
    // Issue API

    @PostMapping("/addIssues") //TODO: make real impl.
    public @ResponseBody List<AngularIssueModel> addIssue(@RequestBody List<AngularIssueModel> issues){
        issuesDAOImpl.issueBooks(issues);
        return issues;
    }

    @PostMapping("/returnBook") //TODO: make real impl.
    public Book returnBook(@RequestBody Book book){
        System.out.println(book);
        issuesDAOImpl.returnBook(book);
        return booksDAO.getBookById(book.getId());
    }


}
