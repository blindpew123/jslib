package tk.blindpew123.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tk.blindpew123.data.*;
import tk.blindpew123.models.LibPage;

import javax.validation.Valid;
import java.util.*;

@Controller
public class BookController {
    private BooksDAO booksDAO;

    private static final String PATH_TO_SEARCH_BOOK = "/WEB-INF/jsp/view/FindBook.jsp";
    private static final String PATH_TO_ADD_BOOK = "/WEB-INF/jsp/view/AddBook.jsp";
    private static final String PATH_TO_LIST_BOOKS =  "/WEB-INF/jsp/view/BooksCatalog.jsp";
    private static final String INDEX = "/index.jsp";


    @Autowired
    public void setBooksDAO(BooksDAO booksDAO) {
        this.booksDAO = booksDAO;
    }

    @ModelAttribute("book")
    public Book getNewBook(){
        Book book = new Book();
        book.setGenre(new Genre());
        book.setCountry(new Country());
        book.setAuthor(new Author());
        return book;
    }


    @RequestMapping(value="/findBook", method = RequestMethod.GET)
    public String openSearchBookForm(Model model){
        model.addAttribute("book",getNewBook());
        model.addAttribute("libPage", new LibPage("Поиск книги", PATH_TO_SEARCH_BOOK));
        return INDEX;
    }


    @RequestMapping(value="/findBook", method = RequestMethod.POST)
    public String findBook(@ModelAttribute("book")Book book, Model model){
        model.addAttribute("list", booksDAO.getBooksByParam(book,true));
        model.addAttribute("libPage", new LibPage("Каталог книг", PATH_TO_LIST_BOOKS));
        return INDEX;
    }


    @RequestMapping(value="/allBooks", method = RequestMethod.GET)
    public String listBooks(Model model){
        model.addAttribute("list", booksDAO.getBooksByParam(null,true));
        model.addAttribute("libPage", new LibPage("Каталог книг", PATH_TO_LIST_BOOKS));
        return INDEX;
    }

    @RequestMapping(value="/addBook", method = RequestMethod.GET)
    public String getAddBookForm(Model model, @ModelAttribute("book") Book book){
        model.addAttribute("libPage", new LibPage("Добавить книгу", PATH_TO_ADD_BOOK));
        return INDEX;
    }


    @RequestMapping(value="/addBook", method = RequestMethod.POST)
    public String  postAddBookForm(@Valid @ModelAttribute("book")Book book, BindingResult resultBookBind, Model model){
        model.addAttribute("libPage", new LibPage("Добавить книгу", PATH_TO_ADD_BOOK));
        if(!resultBookBind.hasErrors()) {
            booksDAO.addBook(book);
            model.addAttribute("book", getNewBook());
        }
        return INDEX;
    }

}
