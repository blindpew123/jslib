package tk.blindpew123.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import tk.blindpew123.data.*;
import tk.blindpew123.models.LibPage;
import tk.blindpew123.utils.Helper;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.Optional;

@Controller
@SessionAttributes("book")
public class IssueController {

    private static final String PATH_TO_ISSUE_BOOK =  "/WEB-INF/jsp/view/IssueBook.jsp";
    private static final String PATH_TO_ISSUED_SUCCSSES =  "/WEB-INF/jsp/view/IssuedSuccess.jsp";
    private static final String PATH_TO_BOOK_INFO =  "/WEB-INF/jsp/view/BookInfo.jsp";
    private static final String PATH_TO_RETURN_SUCCSSES =  "/WEB-INF/jsp/view/ReturnSuccess.jsp";
    private static final String INDEX = "/index.jsp";

    private BooksDAO booksDAO;
    private StudentsDAO studentsDAO;
    private IssuesDAO issuesDAO;

   @ModelAttribute("book")
    public Book getBook(@PathVariable("id") long id) {
        return booksDAO.getBookById(id);
    }

    @ModelAttribute("student")
    public Student getNewStudent(){
        Student student = new Student();
        student.setGrade(new Grade());
        return student;
    }

    @Autowired
    public void setBooksDAO(BooksDAO booksDAO) {
        this.booksDAO = booksDAO;
    }
    @Autowired
    public void setIssuesDAO(IssuesDAO issuesDAO) {
        this.issuesDAO = issuesDAO;
    }
    @Autowired
    public void setStudentsDAO(StudentsDAO studentsDAO) {
        this.studentsDAO = studentsDAO;
    }

    @RequestMapping(value="/issueBook", method = RequestMethod.GET)
    public String openIssueBookForm(@SessionAttribute("book")Book book, @ModelAttribute("student")Student student, Model model){
        model.addAttribute("libPage", new LibPage("Выбрать ученика",PATH_TO_ISSUE_BOOK ));
        return INDEX;
    }

    @RequestMapping(value="/issueBook", method = RequestMethod.POST)
    public String processIssue(@SessionAttribute("book")Book book, @ModelAttribute("student")Student student, Model model, SessionStatus status){
        List<Student> students = studentsDAO.findStudent(student);
        if(students.size() != 1){ //TODO: Ошибка поиска студента
            model.addAttribute("student", getNewStudent());
            model.addAttribute("studentError",students.isEmpty()? "Не найдено учеников, подходящих под ваш запрос" : "Уточните запрос, под выбранные критерии подходит несколько учеников");
            model.addAttribute("libPage", new LibPage("Выбрать ученика",PATH_TO_ISSUE_BOOK ));
        } else {
            Student foundStudent = students.get(0);
            issuesDAO.issueBook(book,foundStudent);
            status.setComplete();
            model.addAttribute("libPage", new LibPage("Книга выдана",PATH_TO_ISSUED_SUCCSSES ));
        }
        return INDEX;
    }

    @RequestMapping(value="/returnBook", method = RequestMethod.GET)
    public String processIssue(@SessionAttribute("book")Book book, Model model) {
        issuesDAO.returnBook(book);
        model.addAttribute("libPage",new LibPage("Книга возвращена успешно",PATH_TO_RETURN_SUCCSSES));
        return INDEX;
    }

    @RequestMapping(value = "/bookInfo/{id}", method = RequestMethod.GET)
    public String showBookDetail(@PathVariable("id") long id,
                                       @ModelAttribute("book")Book book,
                                       SessionStatus status,
                                       HttpServletRequest request,
                                       Model model){

        if(request.getSession().getAttribute("book")!=null){ //Replace book from other user's actions if it still in session
            request.getSession().setAttribute("book", getBook(id));
        }

        Optional<Issue> issued = Helper.getLastIssue(book);
        if(issued.isPresent()){
            model.addAttribute("issued", true);
            long penalty = Helper.calcPenalty(issued);
            if(penalty > 0) {
                model.addAttribute("debt", penalty);
            }
        }
        model.addAttribute("libPage", new LibPage("Информация о книге", PATH_TO_BOOK_INFO));
        return INDEX;
    }
}
