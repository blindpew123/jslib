package tk.blindpew123.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tk.blindpew123.data.*;
import tk.blindpew123.data.validators.StudentValidator;
import tk.blindpew123.models.LibPage;
import tk.blindpew123.utils.Helper;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class StudentController {
    private static final String PATH_TO_LIST_STUDENTS = "/WEB-INF/jsp/view/AllStudents.jsp";
    private static final String PATH_TO_STUDENT_INFO = "/WEB-INF/jsp/view/StudentInfo.jsp";
    private static final String PATH_TO_ADD_STUDENT = "/WEB-INF/jsp/view/AddStudent.jsp";
    private static final String PATH_TO_FIND_STUDENT = "/WEB-INF/jsp/view/FindStudent.jsp";
    private static final String INDEX = "/index.jsp";

    private StudentsDAO studentsDAO;
    private  IssuesDAO issuesDAO;

    @InitBinder("student")
    protected void initBinder(WebDataBinder binder){
        binder.setValidator(new StudentValidator());
    }


    @ModelAttribute("student")
    public Student getNewStudentObj(){
        Student student = new Student();
        student.setGrade(new Grade());
        return student;
    }

    @Autowired
    public void setIssuesDAO(IssuesDAO issuesDAO){
        this.issuesDAO = issuesDAO;
    }

    @Autowired
    public void setStudentsDAO(StudentsDAO studentsDAO) {
        this.studentsDAO = studentsDAO;
    }

    @RequestMapping(value="/allStudents", method = RequestMethod.GET)
    public String listStudents(Model model){
        model.addAttribute("list", studentsDAO.findStudent(new Student()));
        model.addAttribute("libPage", new LibPage("Список учеников", PATH_TO_LIST_STUDENTS));
        return INDEX;
    }

    @RequestMapping(value = "/addStudent", method = RequestMethod.GET)
    public String openAddStudentPage(Model model, @ModelAttribute("student") Student student){
        model.addAttribute("libPage", new LibPage("Добавить ученика", PATH_TO_ADD_STUDENT));
        return INDEX;
    }
    @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
    public String addStudent(@Valid @ModelAttribute("student")Student student, BindingResult studentBindResult, Model model){
        model.addAttribute("libPage", new LibPage("Добавить ученика", PATH_TO_ADD_STUDENT));
        if(!studentBindResult.hasErrors()){
            model.addAttribute("student", getNewStudentObj());
            studentsDAO.addStudent(student);
        }
        return INDEX;
    }

    @RequestMapping(value = "/findStudent", method = RequestMethod.GET)
    public String openFormSearchStudent(Model model, @ModelAttribute("student")Student student){
        model.addAttribute("libPage", new LibPage("Найти ученика", PATH_TO_FIND_STUDENT));
        return INDEX;
    }

    @RequestMapping(value = "/findStudent", method = RequestMethod.POST)
    public String searchStudent(@ModelAttribute("student")Student student, Model model){
        List<Student> students = studentsDAO.findStudent(student);
        if(students.isEmpty()){
            // TODO: Сообщить об ошибке
            model.addAttribute("libPage", new LibPage("Найти ученика", PATH_TO_FIND_STUDENT));
            return INDEX;
        }
        return "redirect:/studentInfo/"+students.get(0).getId();
    }

    @RequestMapping(value = "/studentInfo/{id}", method = RequestMethod.GET)
    public String showStudentInfo(@PathVariable("id") long id, Model model){

        Student student = studentsDAO.findStudentById(id);
        List<Book> list =  issuesDAO.getAllBooksIsssuedTo(student, true);

        model.addAttribute("list", list);
        model.addAttribute("student",student);

        Long totalDebt = list.stream().map(Helper::getLastIssue).filter(Optional::isPresent).mapToLong(Helper::calcPenalty).sum();
        if(totalDebt > 0){
            model.addAttribute("debt",totalDebt);
        }
        model.addAttribute("libPage",new LibPage("Cведения об ученике", PATH_TO_STUDENT_INFO));
        return INDEX;
    }
}
