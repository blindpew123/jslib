package tk.blindpew123.utils;

import tk.blindpew123.data.Book;
import tk.blindpew123.data.Issue;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Optional;

public class Helper {
    public static boolean isValidString(String str){
        return str!=null && !str.isEmpty();
    }

    public static long calcPenalty(Optional<Issue> notReturned){
        long penalty = 0;
        if (LocalDateTime.now().getYear() != notReturned.get().getIssueDateTime().getYear()) {
            penalty = ChronoUnit.DAYS.between(notReturned.get().getIssueDateTime().toLocalDate(), LocalDateTime.now().toLocalDate()) * 10;
        }
        return penalty;
    }

    public static Optional<Issue> getLastIssue(Book book){
        return  book.getBookIssues().stream()
                .sorted(Comparator.comparing((c->c.getIssueDateTime()), Comparator.naturalOrder()))
                .filter(i->i.getReturnDateTime()== null)
                .findFirst();
    }
}
