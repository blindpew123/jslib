package tk.blindpew123.data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.utils.Helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class StudentDaoImpl implements StudentsDAO {

    private static final String FIND_GRADE_QUERY = "FROM Grade G WHERE G = :grade";
    private static final String FIND_STUDENT_PART_QUERY = "FROM Student S JOIN FETCH S.grade Grade WHERE ";
    private static final String SELECT_ALL_STUDENTS_QUERY = "FROM Student S JOIN FETCH S.grade Grade";
    private static final String ORDER_CLAUSE = " ORDER BY S.lastName";
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public void addStudent(Student student) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(FIND_GRADE_QUERY);
        Grade grade = student.getGrade();
        query.setParameter("grade", grade);
        List<Grade> results = query.list();
        if(results.isEmpty()){
            session.persist(grade);
        } else {
            grade = results.get(0);
        }
        student.setGrade(grade);
        session.persist(student);
    }

    @Override
    @Transactional
    public List<Student> findStudent(Student student) {
        Query query = getQueryToFindStudentByParams(student);
        if(query == null){
            return new ArrayList<>();
        }
        return query.list();
    }

    @Transactional
    @Override
    public Student findStudentById(long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Student.class,id);
    }

    /**
     * Build Query to search Student
     * @param student Student properties are used to build a query
     * @return Query to search student or null if can't build query;
     */
    private Query getQueryToFindStudentByParams(Student student){
        if(student==null){
            return null;
        }
        final int initSize = FIND_STUDENT_PART_QUERY.length();
        StringBuilder sb = new StringBuilder(FIND_STUDENT_PART_QUERY);
        if(Helper.isValidString(student.getFirstName())) {
            sb.append("S.firstName = ").append('\'').append(student.getFirstName()).append('\'');
        }
        if(Helper.isValidString(student.getLastName())){
            andHelper(sb,initSize);
            sb.append("S.lastName = ").append('\'').append(student.getLastName()).append('\'');
        }
        if(student.getGrade()!=null && Helper.isValidString(student.getGrade().getGrade())) {
            andHelper(sb,initSize);
            sb.append("S.grade = ").append('\'').append(student.getGrade().getGrade()).append('\'');
        }
        Query<Student> result = null;
        if (sb.length()==initSize){
            result = sessionFactory.getCurrentSession().createQuery(SELECT_ALL_STUDENTS_QUERY + ORDER_CLAUSE);
        } else {
            result = sessionFactory.getCurrentSession().createQuery(sb.append(ORDER_CLAUSE).toString());
        }
        return result;
    }

    private void andHelper(StringBuilder sb, int initSize){
        if (sb.length() > initSize){
            sb.append(" AND ");
        }
    }


}
