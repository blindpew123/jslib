package tk.blindpew123.data;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.utils.Helper;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class BooksDAOImpl implements BooksDAO {
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    @Override
    public void addBook(Book book) {
        Session session = this.sessionFactory.getCurrentSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Author> query = builder.createQuery(Author.class);
        Root<Author> root = query.from(Author.class);
        query.select(root).where(
                builder.equal(root.get("firstName"), book.getAuthor().getFirstName()),
                builder.equal(root.get("lastName"), book.getAuthor().getLastName()));
        Query<Author> authorByNames = session.createQuery(query);
        List<Author> dbAuthors = authorByNames.list();

        if (dbAuthors.isEmpty()) {
            session.persist(book.getAuthor());
        } else {
            book.setAuthor(dbAuthors.get(0)); //impudently suppose that we get only one result. In the opposite case - we need more info about authors and different dbtable and so on...
        }

        if (session.get(Genre.class, book.getGenre().getGenre()) == null) {
            System.out.println("persist");
            session.persist(book.getGenre());
        } else {
            book.setGenre(session.get(Genre.class, book.getGenre().getGenre()));
        }

        if (session.get(Country.class, book.getCountry().getCountry()) == null) {
            session.persist(book.getCountry());
        } else {
            book.setCountry(session.get(Country.class, book.getCountry().getCountry()));
        }
        session.persist(book);
    }

     /**
     * Books' searching
     *
     * @param book        Template for search
     * @param isJoinIssue true if we need info about issues (because by default one-to-many relation uses LAZY load)
     * @return List<Book> with search result
     */
    @Override
    public List<Book> getBooksByParam(Book book, boolean isJoinIssue) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Book> query = builder.createQuery(Book.class);
        Root<Book> root = query.from(Book.class);

        if (isJoinIssue) {
            root.fetch(tk.blindpew123.data.Book_.bookIssues, JoinType.LEFT);
        }
        Predicate[] searchCriteria = getBooksSearchCondition(book, root, builder);
        if (searchCriteria.length == 0) {
            query.select(root);
        } else {
            query.select(root).where(searchCriteria);
        }

        Query<Book> booksWithCriteria = session.createQuery(query);
        List<Book> result = booksWithCriteria.list();
        return result;
    }

    @Override
    public Book getBookById(long id) {
        Session session = sessionFactory.getCurrentSession();
        Book book = session.get(Book.class, id);
        if(book.getBookIssues()!=null){
            Hibernate.initialize(book.getBookIssues());
        }
        return book;
    }


    private Predicate[] getBooksSearchCondition(Book book, Root<Book> root, CriteriaBuilder builder) {
        List<Predicate> list = new ArrayList<>();
        if (book != null) {
            if (Helper.isValidString(book.getBookName())) {
                list.add(builder.equal(root.get(tk.blindpew123.data.Book_.bookName), book.getBookName()));
            }
            if (book.getAuthor() != null && Helper.isValidString(book.getAuthor().getLastName())) {
                list.add(builder.equal(root.get(tk.blindpew123.data.Book_.author).get(tk.blindpew123.data.Author_.lastName), book.getAuthor().getLastName()));
            }
            if (book.getAuthor() != null && Helper.isValidString(book.getAuthor().getFirstName())) {
                list.add(builder.equal(root.get(tk.blindpew123.data.Book_.author).get(tk.blindpew123.data.Author_.firstName), book.getAuthor().getFirstName()));
            }
            if (book.getCountry() != null && Helper.isValidString(book.getCountry().getCountry())) {
                list.add(builder.equal(root.get(tk.blindpew123.data.Book_.country).get(tk.blindpew123.data.Country_.country), book.getCountry().getCountry()));
            }
            if (book.getGenre() != null && Helper.isValidString(book.getGenre().getGenre())) {
                list.add(builder.equal(root.get(tk.blindpew123.data.Book_.genre).get(tk.blindpew123.data.Genre_.genre), book.getGenre().getGenre()));
            }
        }
        return list.toArray(new Predicate[0]);
    }


}