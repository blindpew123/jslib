package tk.blindpew123.data;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

import javax.validation.constraints.NotNull;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "countries")
public class Country {

    @NotEmpty(message="{country.country.empty}")
    private String country;
    private List<Book> countrysBooks;

    @Id
    @Column(name = "country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

   /* @OneToMany (mappedBy="country", targetEntity=Book.class, fetch=FetchType.LAZY)
    @JsonBackReference
    public List<Book> getCountrysBooks() {
        return countrysBooks;
    }

    public void setCountrysBooks(List<Book> countrysBooks) {
        this.countrysBooks = countrysBooks;
    }*/

   @Override
    public String toString(){
        return country;
    }


}
