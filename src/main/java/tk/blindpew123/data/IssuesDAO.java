package tk.blindpew123.data;

import tk.blindpew123.models.AngularIssueModel;

import java.util.List;

public interface IssuesDAO {
    public void issueBook(Book book, Student student);
    public void returnBook(Book book);
    public boolean isBookIssued(Book book);
    public List<Book> getAllBooksIsssuedTo(Student student, boolean isNotReturned);
    public Student getStudentFromIssue (long issueId);
    public void issueBooks(List<AngularIssueModel> issuesDataTuple);

}
