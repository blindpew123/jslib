package tk.blindpew123.data;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "issues")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Issue.class)
public class Issue {

    private long id;
    private Book book;
    private Student student;
    private LocalDateTime issueDateTime;
    private LocalDateTime returnDateTime;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "issueid")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(targetEntity = Book.class, cascade = CascadeType.PERSIST)
    @JoinColumn (name="book")
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @ManyToOne(optional=false, targetEntity = Student.class, cascade = CascadeType.PERSIST)
    @JoinColumn (name="student")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Column(name = "issuedatetime")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    public LocalDateTime getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(LocalDateTime issueDateTime) {
        this.issueDateTime = issueDateTime;
    }
    @Column(name = "returndatetime")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    public LocalDateTime getReturnDateTime() {
        return returnDateTime;
    }

    public void setReturnDateTime(LocalDateTime returnDateTime) {
        this.returnDateTime = returnDateTime;
    }
}
