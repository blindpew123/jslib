package tk.blindpew123.data;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.validator.constraints.NotEmpty;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;


import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "books")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Book.class)
public class Book {


    private long id;

    @NotEmpty(message="{book.bookName.empty}")
    private String bookName;

    @Valid
    private Author author;

    @Valid
    private Genre genre;

    @Valid
    private Country country;


    private List<Issue> bookIssues;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "bookid")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "bookname")
    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @ManyToOne(optional=false, targetEntity = Author.class, cascade = CascadeType.PERSIST)
    @JoinColumn (name="author")
  //  @JsonManagedReference
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @ManyToOne(optional=false, targetEntity = Genre.class, cascade = CascadeType.PERSIST)
    @JoinColumn (name="genre")
 //   @JsonManagedReference
    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @ManyToOne (optional=false,  targetEntity = Country.class, cascade = CascadeType.PERSIST)
    @JoinColumn (name="country")
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString(){
        return id+" "+bookName+" "+author+" "+genre+" "+country;
    }

    @OneToMany(mappedBy = "book")
    public List<Issue> getBookIssues() {
        return bookIssues;
    }

    public void setBookIssues(List<Issue> bookIssues) {
        this.bookIssues = bookIssues;
    }
}
