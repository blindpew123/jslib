package tk.blindpew123.data.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import tk.blindpew123.data.Student;

public class StudentValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Student.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "firstName.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "lastName.empty");
        if(!isAllowedGradePattern(((Student)o).getGrade().getGrade())){
            errors.rejectValue("grade.grade", "grade.checkPattern");
        }
    }

    private boolean isAllowedGradePattern(String gradeStr){
        if(gradeStr!= null && !gradeStr.isEmpty()) {
            char[] chars = gradeStr.toUpperCase().toCharArray();
            if ((chars.length) == 2){
                if(Character.isDigit(chars[0]) && chars[0]!='0'){
                    if(isAllowedLetter(chars[1])) {
                        return true;
                    }
                }
            } else if ((chars.length) == 3){
                if (chars[0]=='1' && '0' <= chars[1] && chars[1] <='1'){
                    if(isAllowedLetter(chars[2])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean isAllowedLetter(char aChar){
        return 'А' <=aChar && aChar <= 'Д';
    }
}
