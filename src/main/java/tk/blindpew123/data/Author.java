package tk.blindpew123.data;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "authors")
public class Author {

    private long id;

    @NotEmpty(message="{author.firstName.empty}")
    private String firstName;

    @NotEmpty(message="{author.lastName.empty}")
    private String lastName;
    private List<Book> authorsBooks;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "authorid")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    @Column(name = "authorfirstname")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "authorlastname")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

   /* @OneToMany (mappedBy="author", targetEntity=Book.class, fetch=FetchType.LAZY)
    @JsonBackReference
    public List<Book> getAuthorsBooks() {
        return authorsBooks;
    }

    public void setAuthorsBooks(List<Book> authorsBooks) {
        this.authorsBooks = authorsBooks;
    } */

    @Override
    public String toString(){
        return firstName+" "+lastName;
    }
}
