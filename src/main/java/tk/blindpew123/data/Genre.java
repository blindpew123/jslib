package tk.blindpew123.data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "genres")
public class Genre {

    @NotEmpty(message="{genre.genre.empty}")
    private String genre;
    private List<Book> genresBooks;

    @Id
    @Column(name = "genre")
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    /*@OneToMany (mappedBy="genre", targetEntity=Book.class, fetch=FetchType.LAZY)
    @JsonBackReference
    public List<Book> getGenresBooks() {
        return genresBooks;
    }

    public void setGenresBooks(List<Book> genresBooks) {
        this.genresBooks = genresBooks;
    } */
    @Override
    public String toString(){
        return genre;
    }


}
