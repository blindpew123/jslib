package tk.blindpew123.data;

import java.util.List;
import java.util.Map;

public interface StudentsDAO {
    void addStudent(Student student);
    List<Student> findStudent(Student studentTemplate);
    Student findStudentById(long id);

}
