package tk.blindpew123.data;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "students")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id",
        scope = Student.class)
public class Student {

    private long id;

    private String firstName;
    private String lastName;
    private Grade grade;
    private List<Issue> studentIssues;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "studentid")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "firstname")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "lastname")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @ManyToOne(optional=false, cascade = CascadeType.PERSIST)
    @JoinColumn (name="grade")
    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    /*
    @OneToMany(mappedBy = "student", targetEntity = Issue.class, fetch = FetchType.LAZY)
    public List<Issue> getStudentIssues() {
        return studentIssues;
    }

    public void setStudentIssues(List<Issue> studentIssues) {
        this.studentIssues = studentIssues;
    }
    */

    @Override
    public String toString() {
        return firstName + " " + lastName + " "+ grade.getGrade();
    }


}
