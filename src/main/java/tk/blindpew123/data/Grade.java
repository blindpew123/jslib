package tk.blindpew123.data;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "grades")
public class Grade {
    private String grade;
    private List<Student> gradeStudents;

    @Id
    @Column(name = "grade")
    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

   /* @OneToMany(mappedBy = "grade", targetEntity = Student.class, fetch = FetchType.LAZY)
    @JsonBackReference
    public List<Student> getGradeStudents() {
        return gradeStudents;
    }

    public void setGradeStudents(List<Student> gradeStudents) {
        this.gradeStudents = gradeStudents;
    }*/

    @Override
    public String toString() {
        return grade;
    }
}