package tk.blindpew123.data;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.models.AngularIssueModel;


import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Service
public class IssuesDAOImpl implements IssuesDAO {

    private SessionFactory sessionFactory;

    private static final String FIND_RECORDS_ABOUT_BOOK_ISSUE = "FROM Issue I WHERE I.book = :book ORDER BY I.issueDateTime";
    private static final String IS_BOOK_NOT_RETURNED = "FROM Issue I WHERE I.book = :book AND I.returnDateTime is NULL";
    private static final String ALL_BOOKS_ISSUED_TO_STUDENT = "FROM Book B LEFT OUTER JOIN FETCH B.bookIssues as I WHERE I.student.id = :id ORDER BY I.issueDateTime";
  //  private static final String ALL_BOOKS_ISSUED_TO_STUDENT = "FROM Book B LEFT OUTER JOIN FETCH B.bookIssues as I WHERE I.student = :student ORDER BY I.issueDateTime";
    private static final String ALL_BOOKS_ISSUED_TO_STUDENT_WITHOUT_RETURN
            = "FROM Book B LEFT OUTER JOIN FETCH B.bookIssues as I WHERE  I.student = :student AND I.returnDateTime is null ORDER BY I.issueDateTime";
    private static final String GET_STUDENT_FROM_ISSUE = "FROM Student S LEFT OUTER JOIN FETCH S.studentIssues as I WHERE I.id = :id ";

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Writes record about issue book, into table, sets timestamp
     * @param book Issued Book, must be checked before, that it is not contained in issuance table without retutn timestamp
     *             else throws IllegalArgumentException
     */

    @Transactional
    @Override
    public void issueBook(Book book, Student student) {
        Session session = sessionFactory.getCurrentSession();
        if(book == null || isBookIssued(book)){
            throw new IllegalArgumentException();
        }
        Issue issue = new Issue();
        issue.setBook(session.get(Book.class,book.getId()));
        issue.setStudent(session.get(Student.class,student.getId()));
        issue.setIssueDateTime(LocalDateTime.now());
        session.persist(issue);
    }
    @Transactional
    @Override
    public void returnBook(Book book) {
        Session session = sessionFactory.getCurrentSession();
        Query<Issue> query = session.createQuery(IS_BOOK_NOT_RETURNED);
        query.setParameter("book",book);
        List<Issue> issues = query.list();
        System.out.println(issues);
        if(issues.isEmpty() || issues.size()>1){
            throw new IllegalArgumentException("Book "+book +" was issue more than one time");
        }
        Issue issue = issues.get(0);
        issue.setReturnDateTime(LocalDateTime.now());
        session.update(issue);
    }
    @Transactional
    @Override
    public boolean isBookIssued(Book book) {
        List<Issue> issues = getRecordsAboutBookIssue(book);
        if(!issues.isEmpty() && issues.get(issues.size()-1).getReturnDateTime() == null){
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public List<Book> getAllBooksIsssuedTo(Student student, boolean isNotReturned) {
        Query<Book> query = sessionFactory.getCurrentSession()
                .createQuery(isNotReturned ? ALL_BOOKS_ISSUED_TO_STUDENT_WITHOUT_RETURN : ALL_BOOKS_ISSUED_TO_STUDENT);
      //  query.setParameter("student", student);
        query.setParameter("id", student.getId());
        return query.list();
    }

    @Transactional
    @Override
    public Student getStudentFromIssue(long issueId) {
        Query<Student> query = sessionFactory.getCurrentSession().createQuery(GET_STUDENT_FROM_ISSUE);
        query.setParameter("id", issueId);
        Student student = query.list().get(0);
        return student;
    }

    @Transactional
    @Override
    public void issueBooks(List<AngularIssueModel> issuesDataTuple) {
        Session session = sessionFactory.getCurrentSession();
        Student student = null;
        for(AngularIssueModel idTuple: issuesDataTuple) {
            Issue issue = new Issue();
            if (student == null) {
                student = session.get(Student.class, idTuple.getStudentId());
            }
            issue.setBook(session.get(Book.class, idTuple.getBookId()));
            issue.setStudent(student);
            issue.setIssueDateTime(LocalDateTime.now());
            session.persist(issue);
        }
    }

    private List<Issue> getRecordsAboutBookIssue(Book book){
        Session session =  sessionFactory.getCurrentSession();
        Query<Issue> query = session.createQuery(FIND_RECORDS_ABOUT_BOOK_ISSUE);
        query.setParameter("book", book);
        return query.list();
    }
}
