package tk.blindpew123.data;

import java.util.List;
import java.util.Map;

public interface BooksDAO {
    public void addBook(Book book);
    public List<Book> getBooksByParam(Book book, boolean isJoinIssue);
    public Book getBookById(long id);
}
