package tk.blindpew123.models;

public class LibPage {
    private String title;
    private String contentURL;

    public String getTitle() {
        return title;
    }

    public LibPage(){};
    public LibPage(String title, String contentURL){
        this.contentURL = contentURL;
        this.title = title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentURL() {
        return contentURL;
    }

    public void setContentURL(String contentURL) {
        this.contentURL = contentURL;
    }
}
